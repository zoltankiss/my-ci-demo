# Specify Vagrant version and Vagrant API version
Vagrant.require_version ">= 1.7.0"
VAGRANTFILE_API_VERSION = "2"
HOST_NAME = 'ci-demo-vagrant-machine'

# Create and configure the VM(s)
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.provider "virtualbox" do |v|
    v.memory = 8_192
    v.cpus = 4
    v.name = HOST_NAME
  end

  # Spin up a "host box" for use with the Docker provider
  # and then provision it with Docker
  config.vm.box = 'ubuntu/trusty64'

  # Assign a friendly name to this host VM
  config.vm.hostname = HOST_NAME

  #-----------------------------------#
  #  Setup Host/Guest folder syncing  #
  #-----------------------------------#

  config.ssh.forward_agent = true


  #-------------------------------#
  #  Setup Host/Guest networking  #
  #-------------------------------#

  port_forwards = [
  ]

  port_forwards.each do |ports|
    config.vm.network :forwarded_port, guest: ports[:guest], host: ports[:host], host_ip: ports[:host_ip]
  end

  config.vm.network :private_network, type: :dhcp


  #-------------------------------#
  #  Start Provisioning Machine   #
  #-------------------------------#

  # Force the ssh connection to reconnect to use the new docker group the vagrant user has been added to
  config.vm.provision "shell", inline: "ps aux | grep 'sshd: vagrant' | awk '{print $2}' | xargs kill"
end

