Rails.application.routes.draw do
  get '/test' => 'tests#test', as: 'test'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
