# frozen_string_literal: true

require 'rails_helper'

feature 'test', js: true do
  scenario 'test' do
    visit test_path

    expect(page).to have_content("hello world!")
    expect(page).to have_content("Hello Vue!")
  end
end
