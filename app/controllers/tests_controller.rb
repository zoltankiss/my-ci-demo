require 'net/http'

class TestsController < ApplicationController
  def test
    @title = JSON.parse(Net::HTTP.get(API_HOST, '/test', 3013))["message"]
  end
end
